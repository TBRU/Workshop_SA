library(ggtree)
library(ape)
library(ggplot2)
library("phytools")

Tree_File = "RAxML_bestTree.GTR_GAMMA"


Tree = read.tree(Tree_File)
Tree <- root(Tree,"MTB_ANC",resolve.root= TRUE)     #reroot tree using ancestor

p <- ggtree(Tree,layout = "rectangular",size=.2)+ ggtitle("DR")

DR_file = "~/Workshop_SA/scripts_phylogeny/all_DRM_in_Eldholm.txt_transposed"
DR_annotation = read.table(DR_file,sep='\t',header=T,stringsAsFactors = F)



pdf("DR_tree.pdf")
gheatmap(p, DR_annotation,width = 0.8,font.size = 1.3,hjust = 1,colnames=TRUE,colnames_position = "top",colnames_angle = 270) +
  scale_fill_manual(values=c("0/0"='gray90','1/1'="blue", "0/1" = "gray1"))


dev.off()

