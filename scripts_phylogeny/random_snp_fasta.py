
from Bio import SeqIO
import argparse
import re

import random

parser = argparse.ArgumentParser()

parser.add_argument('INFILE',type=str,help='path to the multi fasta alignment')
parser.add_argument('-n','--number_pos', metavar='INT', default='', help='number of positions to select', type =int,nargs='?')





arguments = parser.parse_args()

fasta={}
out={}

for record in SeqIO.parse(arguments.INFILE, "fasta"):
	length = len(record.seq)
	fasta.update({str(record.id):str(record.seq)})
	out.update({str(record.id):""})
if (arguments.number_pos > length):
	raise argparse.ArgumentTypeError("there are not so many nucleotide in the infile")



n=random.sample(range(0, length), arguments.number_pos)

for i in n:
	
	for name in fasta:
		
		out[name] += fasta[name][i]

#print result

for name_out, seq_out in out.items():
	print ">" + name_out
	print seq_out
	#scp print len(seq_out)








