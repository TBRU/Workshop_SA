
from Bio import SeqIO
import argparse
import re

############################################################			define arg type float 0 < X > 1		###############################################################

def restricted_float(x):
    x = float(x)
    if x < 0.0 or x > 1.0:
        raise argparse.ArgumentTypeError("%r not in range [0.0, 1.0]"%(x,))
    return x


parser = argparse.ArgumentParser()

parser.add_argument('INFILE',type=str,help='path to the multi fasta alignment')
parser.add_argument('-m','--missing', metavar='0-1', default='0.9', help='minimum percentage of NOT missing data', type =restricted_float,nargs='?')

arguments = parser.parse_args()

fasta={}


for record in SeqIO.parse(arguments.INFILE, "fasta"):
	length = len(record.seq)
	good_seq_length=len(re.findall('[ATCGatcg]', str(record.seq)))
	ratio= float(good_seq_length)/float(length)
	if (ratio >= arguments.missing):	
		fasta.update({str(record.id) : str(record.seq)})






#print result

for name_out, seq_out in fasta.items():
	print (">" + name_out)
	print (seq_out)
	#scp print len(seq_out)








