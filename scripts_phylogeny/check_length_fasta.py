
from Bio import SeqIO
import argparse
import re

############################################################			define arg type float 0 < X > 1		###############################################################

def restricted_float(x):
    x = float(x)
    if x < 0.0 or x > 1.0:
        raise argparse.ArgumentTypeError("%r not in range [0.0, 1.0]"%(x,))
    return x


parser = argparse.ArgumentParser()

parser.add_argument('INFILE',type=str,help='path to the multi fasta alignment')
parser.add_argument('-m','--missing', metavar='0-1', default='0.9', help='minimum percentage of NOT missing data', type =restricted_float,nargs='?')

arguments = parser.parse_args()

fasta={}


for record in SeqIO.parse(arguments.INFILE, "fasta"):
	length = len(record.seq)
	print (length)







